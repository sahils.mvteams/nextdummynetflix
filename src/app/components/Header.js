"use client"
import React from 'react'
import {Container,Nav,Navbar,Row,Col} from 'react-bootstrap'
import Link from 'next/link'
import '../styles/Header.css'
export default function Header() {
  return (
    <div style={{backgroundColor:'black', height:'100px', width:'100%'}}>
        <div className='col-md-12 col-sm-12 row'>
            <div className='col-md-3 col-sm-3 mt-2'>
                <h1 className='logo'>Dummy Flix</h1>
            </div>
            <div className='col-md-9 col-sm-9 mt-4' style={{textAlign:'right'}}>
                <div >
                    <Link href='/' className='mx-5 nav-links'>Home</Link>
                    <Link href='/movies' className='mx-5 nav-links'>Movies</Link>
                    <Link href='/contact' className='mx-5 nav-links'>Contact</Link>
                </div>  
            </div>
        </div>  
        
    </div>
  )
}
