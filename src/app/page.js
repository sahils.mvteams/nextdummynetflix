import React from 'react'
import Home from './components/Home'
import styles from "./styles/common.module.css";

export default function Page() {
  return (
    <div className={styles.home}>
      <Home />
    </div >
  )
}
